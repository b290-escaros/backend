// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions

const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {
  return Task.find({}).then(result => {
    return result;
  });
};

module.exports.createTask = (requestBody) => {
  console.log(requestBody);

  let newTask = new Task({
    name: requestBody.name
  });
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return task;
    }
  });
};

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then(removedTask => {
    return removedTask;
  }).catch(err => {
    console.log(err);
    return false;
  });
};


module.exports.updateTask = (taskId, newContent) => {
  console.log(newContent)
  return Task.findById(taskId).then(result => {
    result.status = newContent.status; 
    return result.save().then((updatedTask, saveErr) => {
      if(saveErr){
        console.log(saveErr);
        return false
      } else {
        return updatedTask;
      }
    })
  }).catch(err => console.log(err));
}

// s36 Activity

// Function for getting a specific task
module.exports.getTask = (taskId) => {
  return Task.findById(taskId)
    .then(result => {
      return result;
    })
    .catch(error => {
      console.log(error);
      return false;
    });
};

// Function for changing the status of a task to complete
module.exports.completeTask = (taskId) => {
  return Task.findByIdAndUpdate(
    taskId,
    { status: "complete" },
    { new: true }
  )
    .then(updatedTask => {
      return updatedTask;
    })
    .catch(error => {
      console.log(error);
      return false;
    });
};

