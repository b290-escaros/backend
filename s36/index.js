// Setting up dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js")

// Server setup
const app = express();
const port = 4000;

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5h7tglz.mongodb.net/b290-to-do?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("MongoDB Atlas Connected"));

// Setup Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setup Routes
app.use("/tasks", taskRoute);

// Server Listen
if (require.main === module) {
  app.listen(port, () => console.log(`Server running at ${port}`));
}

module.exports = app;
