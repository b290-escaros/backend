// console.log("Hello")

// Section : Arithmitic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operators: " + sum)

/*Mini Activity
*/

let difference = x - y;
console.log("Result of subtraction operators: " + difference);

let quotient = x / y;
console.log("Result of addition Quotient operators: " + quotient)

let product = x * y;
console.log("Result of product operation: " + product)

let modulos = x % y;
console.log("Result of product operation: " + modulos)

// Section 
// Basic Assignment operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition Assignment operator(+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
assignmentNumber = assignmentNumber + 2;
console.log("Result " + assignmentNumber);

//Shorthand for assigning
assignmentNumber += 2
console.log("Result addition Assignment operator " + assignmentNumber)


assignmentNumber -= 2
console.log("Result subtraction Assignment operator " + assignmentNumber)

assignmentNumber *= 2
console.log("Result multiplication Assignment operator " + assignmentNumber)

assignmentNumber /= 2
console.log("Result division Assignment operator " + assignmentNumber)

assignmentNumber %= 3
console.log("Result modulo Assignment operator " + assignmentNumber)


/*Multile Operators and Parenthesis
- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
*/

let mdas =  1+2-3*4/5;
console.log("Result of mdas operation:" + mdas);

/*
- By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
- The operations were done in the following order:
    1. 4 / 5 = 0.8
    2. 2 - 3 = -1
    3. -1 * 0.8 = -0.8
    4. 1 + -.08 = .2
*/

let pemdas = 1+(2-3)*(4/5);
console.log("Result of pemdas operation: " + pemdas)

let pemdas2 = (1+(2-3))*(4/5);
console.log("Result of pemdas2 operation: " + pemdas2)

//Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
let increment = ++z;
console.log("result of pre-increment: " + increment);
// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
console.log("result of pre-increment: " + z);
// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
// The value of "z" is at 2 before it was incremented
console.log("result of post-increment: " + increment)
/*The value of 'z' was increased again reaassigning the value to 3*/
console.log("result of post-increment: " + z);

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
let decrement = --z;
// The value of "z" is at 3 before it was decremented
console.log("result of pre-decrement: " + decrement);
// The value of "z" was decreased reassigning the value to 2
console.log("result of pre-decrement: " + z);

decrement = z--;
// The value of "z" is at 2 before it was decremented
console.log("result of post-decrement: " + decrement);
console.log("result of post-decrement: " + z);

//Section Comparison Operators
let juan = "Juan"

// Equality Operator
/* 
- Checks whether the operands are equal/have the same content
- Attempts to CONVERT AND COMPARE operands of different data types
- Returns a boolean value
*/

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

// Strict Equality Operator
/* 
- Checks whether the operands are equal/have the same content
- Also COMPARES the data types of 2 values
- JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
- In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
- Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === "juan");
console.log("juan" === juan);

// Inequality Operator
/* 
- Checks whether the operands are not equal/have different content
- Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != "juan");
console.log("juan" != juan);

// Strict Inequality
/* 
- Checks whether the operands are not equal/have the same content
- Also COMPARES the data types of 2 values
*/
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Section Relational Operators

//Some comparison operators check whether one value is greater or less than to the other value.
let a = 50;
let b = 65;
/*GT or greater than operator(>)*/
let isGreaterThan = a > b;
/*LT or Less than operator (<)*/
let isLessThan = a < b;
/*GTF Greater than or Equal operator (>=)*/
let isGreaterThanOrEqual = a >= b;
/*LTE (<=) */
let isLessThanOrEqual = a <= b;
console.log(isGreaterThan)
console.log(isLessThan)
console.log(isGreaterThanOrEqual)
console.log(isLessThanOrEqual)

let numstr = "30";
console.log(a > numstr); // True - Converted String to a number
console.log(a <= numstr);

let str = "twety"
/*console.log(b >= str); false - str resulted to Nan string is not numric
*/

// Section Logical Operators
let isLegalAge = true;
let isRegistered = false;

// AND  Operator (&& - Double Ampersand)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: " + allRequirementsMet);

// OR Operator (|| - Double Pipe/Bar)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical AND Operator: " + someRequirementsMet);

// NOt Operator (! Exclamation point)
// Returns the opposite value

let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);