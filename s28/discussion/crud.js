// CRUD Operations

// SECTION] Inserting documents (Create)

// Inserting single doc
// Syntax: db.collectionName.insertOne("Object")
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript","Python"],
	department: "none"
})

// Inserting multiple documents
// Syntax: db.collectionName.insertMany([])

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React","PHP"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel","Sass"],
	department: "none"
}

])
// SECTION] FInding documents (Read)
// Syntax: db.collectionName.find();
/*
	Syntax:
	 db.collectionName.find();
	 db.collectionName.find({field: value});
*/
// leaving the search criteria empty will retrieve all the documents
db.users.find();
db.users.find({firstName: "Neil"});

// Syntax: db.collectionName.find({fieldA: valueA, fieldB: valueB});
// Finding documents with multiple parameter
db.users.find({lastName: "Neil", age: 82});

// SECTION] Updating documents(Update)
// updating a single document
// Syntax: db.collectionName.updateOne({criteria}, {$set: {field: value}})
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
	status: "inactive"
});

db.users.updateOne(
	{firstName: "Test"}, 
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "billgates@gmail.com"
			},
			courses: ["PHP", "Laravel","HTML"],
			department: "none",
			status: "active"
		}
	}
)
// Updating multiple documernts
// SYNTAX: 
	/*
		db.collectionName.updateMany({criteria}, {$set: {field: value}})
	*/

db.users.updateMany(
	{ department: "none" },
	{
		$set{department: "HR"}
	}
)

// ReplaceOne
db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
	}
)

// SECTiON] Deleting documents (Delete)

// Document to be deleted
db.ysers.insertOne({
	firstName: "Test"
});

// Deleting a single documents

/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "Test"});

// Deleting Many
/*
	Syntax:
		db.collectionName.deleteMany({criteria})
*/
db.users.deleteMany({firstName: "Test"});

// SeCTION] Advance queries
db.users.find({
	contact: {
				phone: "09123456789",
				email: "stephenhawking@gmail.com"
			}
})

db.users.find({"contact.email":"janedoe@gmail.com"})


db.users.find({courses:"janedoe@gmail.com"})







db.users.insertOne({
	namearr:[
	{
		nameb: "Juan"
	},
	{
		nameb "TAmad"
	}



		]
})

db.users.find({
	namearr: {
		namea: "Juan"
	}
})