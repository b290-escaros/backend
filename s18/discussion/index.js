// console.log("Hello");

// function printInput(){
// 	let nickName = prompt("Enter your Nickname:" );
// 	return "Hi " + nickName;
// }

// Parameters with prompt the values will be 
function printInput(nickname){
	nickname = prompt("Enter your nick:")
	return "Hi " + nickname;
};

// console.log(printInput("Nicolas"));

// console.log(printInput());

// You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.
/*"name" is called a paramet
function printName(name){
	return "My name is " + name;
};
er.

A "parameter" acts as a named variable/container that exists only inside of a function
It is used to store information that is provided to a function when it is called/invoked.

the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.*/
// console.log(printName("Nicolas"));

// variables can be also passed as an argument
// let anotherName = "Mitsuha";
// console.log(printName(anotherName));

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of:" + num + "divided by 8 is" + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is" + num + "Divisible by 8?");
	console.log(isDivisibleBy8);

}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments
/*	Function parameters can also accept other functions as arguments
	Some complex functions use other functions as arguments to perform more complicated results.
	This will be further seen when we discuss array methods.
*/
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed")
}
function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);
console.log(argumentFunction);

// USING Multiple Parameters
/*Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.*/

function createFullname(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
}
console.log(createFullname("Juan", "Dela", "Cruz"));

// function sum(num1, num2){
// 	const result = num1 + num2;
// 	return `Displayed sum of ${num1} and ${num2}\n${result}`
// }
// console.log(sum(5, 15));
// function sum(num1, num2) {
//   const result = num1 + num2;
//   return `Sum of ${num1} and ${num2} is ${result}`;
// }

// console.log(sum(5, 15));

// function sum(num1, num2) {
//   return num1 + num2;
// }
// console.log(`Displayed sum of 5 and 15 \n${sum(5, 15)}`);

function getArea(radius){
	return Math.PI * radius * radius
}
console.log(getArea(15));