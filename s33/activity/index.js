//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('https://jsonplaceholder.typicode.com/todos')
       .then((response) => response.json())
       .then((json) => console.log(json))
   );

}

// Getting all to do list item
async function getAllToDo(){

   return await (

      //add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => json.map((item, index) => `\n${index}.${item.title}`))
  );
}

// getAllToDo().then((response) => console.log('All To Do:', response.join(', ')));

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1')
            .then((response) => response.json())
            .then((json) => json)
   );

}
// getSpecificToDo().then((response) => console.log('Specific To Do:', response));

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos', {
           method: 'POST',
           body: JSON.stringify({
            completed: false,
             id: 201,
             title: 'Created To Do List Item',
             userId: 1,
           }),
           headers: {
             'Content-type': 'application/json'
           },
         })
           .then((response) => response.json())
           .then((json) => json)
   );
}
// createToDo().then((response) => console.log('Created To Do:', response));

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
            method: 'PUT',
            body: JSON.stringify({
               dateCompleted: 'pending',
               description: 'To update my to do list with different data structure',
               id: 1 ,
               status: "pending",
               title: 'Updated To Do list',
               userId: 1,
            }),
            headers: {
              'Content-type': 'application/json'
            },
          })
            .then((response) => response.json())
            .then((json) => json)
   );

}
// updateToDo().then((response) => console.log('Updated To Do:', response));

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
            method: 'DELETE',
           
            })//.then((response) => response.json())
            //.then((json) => console.log(json))           
   );

}

// deleteToDo().then((response) => console.log('Deleted To Do:', response));


//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}

