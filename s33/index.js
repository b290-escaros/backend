async function fetchData(){
	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// REsult returned by fetch returns a promise
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	let json = await result.json();
	console.log(json);

}

fetchData();

// SECTION] getting a specific post
// Retrieval specific post following the RETS API
// (/posts/:id, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

// SECTION] creating a post
	/*
		Syntax
			fetch("URL", options)
				.then(res =>{})
				.then(res =>{})

	*/
	// create a new post following the REST API 

fetch("https://jsonplaceholder.typicode.com/posts",
		{
			// Sets the method of the "Request" object to "POST" following REST API
			// Default method is GET
			method: "POST",
			// Sets the header data of the "Request" object to be sent to the backend
			// Specified that the content will be in a JSON structure
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				title: "New post",
				body: "Hello World",
				userId: 1
			})

		} 
	).then(response => response.json())
		.then(json => console.log(json));

// SECTION] Updating a post
//  /posts/:id
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "PUT", 
	headers: {
		"Content-Type": "application/json"
	},
	body : JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userID: 1

	})
}).then(response => response.json()).then(json => console.log(json));


// PATCHY
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "PATCH", 
	headers: {
		"Content-Type": "application/json"
	},
	body : JSON.stringify({
		id: 1,
		title: "Patched post",
		body: "Hello again",
		userID: 1

	})
}).then(response => response.json()).then(json => console.log(json));

// DELETE
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "DELETE", 
	headers: {
		"Content-Type": "application/json"
	},

	}).then(response => response.json()).then(json => console.log(json));

// SECTION] Filtering post
// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)

/*
	Syntax:
		Individual parameter
		"url?parameterName=value"
		Multiple parameter
		"url?"paramA=value&paramB=valueB

*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
	.then(response => response.json())
	.then(json => console.log(json));

// [Section] Retrieving nested/related comments to posts

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json))
