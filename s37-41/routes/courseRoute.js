const express = require("express");
const router = express.Router();
const auth = require("../auth"); // Import the auth module for authentication
const courseController = require("../controllers/courseController");


// Route for creating a course (requires authentication and isAdmin)
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    courseController.addCourse(req.body)
      .then(resultFromController => res.send(resultFromController));
  } else {
    console.log({ auth: "unauthorized user" });
    res.send(false);
  }
});


router.get("/all", auth.verify, (req, res) => {  //need for middleware
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false)
	{
		res.send("User is not an admin")
	}else{
		courseController.getAllCourses().then(result => res.send(result))
	}
    
});

router.get("/", (req,res) => {
	courseController.getAllActiveCourses().then(
		resultFromController => res.send(
			resultFromController));
});

router.get("/:courseId", (req, res) => {
  console.log(req.params);
  courseController.getCourse(req.params)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => {
      console.log(error);
      res.sendStatus(500);
    });
});

// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	
router.put("/:courseId", auth.verify, (req,res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	if(isAdmin){
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}else{
		console.log("Unauthorized user")
		res.send(false)
	}

});

// Route for archiving a course (requires authentication and isAdmin)
router.patch("/:courseId", auth.verify, (req, res) => {
  const isAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (isAdmin) {
    courseController.archiveCourse(req.params.courseId)
      .then(resultFromController => res.send(resultFromController));
  } else {
    console.log("Unauthorized user");
    res.send(false);
  }
});


module.exports = router;
