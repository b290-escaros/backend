const express = require("express");
const router = express.Router();
const auth = require("../auth"); // Import the auth module for authentication
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
  userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
  userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details (requires authentication)
router.post("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile({ userId: userData.id })
    .then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course (requires authentication)
router.post("/enroll", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const data = {
    userId: userData.id,
    courseId: req.body.courseId
  };

  if (!data.courseId) {
    return res.status(400).send({ success: false, message: "courseId is required" });
  }

  userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
