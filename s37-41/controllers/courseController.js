const Course = require("../models/Course");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = reqBody => {
  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
    // Add other properties as needed
  });

  // Save the created course to the database
  return newCourse.save()
    .then(courses => true)
    .catch(err => {
      console.log(err);
      return false;
    });
};

module.exports.getAllCourses = () => {
  return Course.find({})
    .then(courses => courses)
    .catch(err => {
      console.log(err);
      return [];
    });
};

module.exports.getAllActiveCourses = () => {
  return Course.find({isActive: true})
    .then(courses => courses)
    .catch(err => {
      console.log(err);
      return [];
    });
};

module.exports.getCourse = reqParams =>{
	return Course.findById(reqParams.courseId).then(result => result).catch(err => {
		console.log(err)
		return false
	})
}

module.exports.updateCourse = (reqParams,reqBody)=>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(err => {
		console.log(err);
		return false
	})
}

// Controller method for archiving a course
module.exports.archiveCourse = courseId => {
  return Course.findByIdAndUpdate(courseId, { isActive: false })
    .then(course => {
      if (course) {
        return true;
      } else {
        return false;
      }
    })
    .catch(err => {
      console.log(err);
      return false;
    });
};
