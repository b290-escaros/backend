const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Course.js");



module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then(result => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
    password: bcrypt.hashSync(reqBody.password, 10)
  });

  // Save the created object to our database
  return newUser.save()
    .then(user => true)
    .catch(err => {
      console.log(err);
      return false;
    });
};

// User authentication (login)


module.exports.loginUser = reqBody => {
  return User.findOne({ email: reqBody.email })
  .then(result => {
    if (result == null) {
      console.log("user not registered")
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        console.log("password not registered")
        return false;
      }
    }
  });
};

// s37-41 activity

// Create a getProfile request
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then(result => {
    if (result) {
      result.password = ""; // Assign an empty string to the password field for security
      return result;
    } else {
      return null;
    }
  });
};


module.exports.enroll = async (data) => {
  if (!data.userId) {
    return { success: false, message: "userId is required" };
  }

  if (!data.courseId) {
    return { success: false, message: "courseId is required" };
  }

  let isUserUpdated, isCourseUpdated;

  try {
    const user = await User.findById(data.userId);
    if (!user) {
      return { success: false, message: "User not found" };
    }

    user.enrollments.push({ courseId: data.courseId });
    isUserUpdated = await user.save().then(user => true);
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error on retrieving user" };
  }

  try {
    const course = await Course.findById(data.courseId);
    if (!course) {
      return { success: false, message: "Course not found" };
    }

    course.enrollees.push({ userId: data.userId });
    isCourseUpdated = await course.save().then(course => true);
  } catch (err) {
    console.log(err);
    return { success: false, message: "Error on retrieving course" };
  }

  return { success: isUserUpdated && isCourseUpdated };
};