const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors");
const courseRoute = require("./routes/courseRoute")

// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings

const app = express();

// Connect MOngoDB Database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5h7tglz.mongodb.net/booking-DB?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."))

// Middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use("/courses", courseRoute)

// Routes
app.use("/users", userRoute);

if(require.main === module){
	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now online on port ${process.env.PORT||4000}`)
	})
}

module.exports = {app, mongoose};