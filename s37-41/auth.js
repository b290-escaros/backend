const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// Token creation
module.exports.createAccessToken = user => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  };
  return jwt.sign(data, secret, {});
};

// Token verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        next();
      }
    });
  } else {
    return res.send({ auth: "failed" });
  }
};

// Middleware for checking if user is an admin
/*module.exports.isAdmin = (req, res, next) => {
  // Add your isAdmin logic here...
  // For example, check if the user's isAdmin property is true
  if (req.user && req.user.isAdmin) {
    return true;
  } else {
    return res.send({ auth: "failed" });
  }
};*/

// Token decoding
module.exports.decode = token => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
