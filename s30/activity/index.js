// S30 - Aggregation in MongoDB and Query Case Studies:

/*

    Sample solution:

    return async function addOneQuery(db) {
        await (db.collectionName.aggregate([
	      { $match: { fieldA, valueA } },
	      { $group: { _id: "$fieldB" }, { result: { operation } } }
    ]));
        
    }

Note: 
	- Do note change the functionName or modify the exports
	- Delete all the comments before pushing.

*/


// 1. Use the count operator to count the total number of fruits on sale
async function fruitsOnSale(db) {
	return await(
			// https://www.mongodb.com/docs/manual/reference/operator/aggregation/count-accumulator/#mongodb-group-grp.-count
			// the $count stage calculates the total count of matching documents and assigns it to the field
			// Add query here
			// Using estimatedDocumentCount (MongoDB 4.0+)
			db.fruits.aggregate([
			  { $match: { onSale: true } },
			  { $count: "fruitsOnSale" }
			])

		);
};


// 2. Use the count operator to count the total number of fruits with stock more than 20
async function fruitsInStock(db) {
	return await(
			// https://www.mongodb.com/docs/manual/reference/operator/aggregation/count-accumulator/#mongodb-group-grp.-count
			// the $count stage calculates the total count of matching documents and assigns it to the field
			// Add query here
		db.fruits.aggregate([
		  { $match: {stock: { $gte: 20 } } },
		  { $count: "enoughStock" }
		])

		);
};


// 3. Use the average operator to get the average price of fruits onSale per supplier
async function fruitsAvePrice(db) {
	return await(
			
			// https://stackoverflow.com/questions/39080534/calculate-a-fields-average-after-mongo-db-aggregate-match-and-lookup-query
			// The $avg operator is used in MongoDB's Aggregation framework to calculate the average value of a specified field across multiple documents.
			// Add query here
		db.fruits.aggregate([
		  { $match: { onSale: true } },
		  {
		    $group: {
		      _id: "$supplier_id",
		      avg_price: { $avg: "$price" }
		    }
		  }
		])

		);
};


// 4. Use the max operator to get the highest price of a fruit per supplier
async function fruitsHighPrice(db) {
	return await(
			// https://www.mongodb.com/docs/manual/reference/operator/aggregation/
			// https://www.digitalocean.com/community/tutorials/how-to-use-aggregations-in-mongodb
			// The $min and $max operators are used in MongoDB's Aggregation framework to find the minimum and maximum values of a specified field, respectively, across multiple documents.
			// Add query here
		db.fruits.aggregate([
		  { $match: { onSale: true } },
		  {
		    $group: {
		      _id: "$supplier_id",
		      max_Price: { $max: "$price" }
		    }
		  }
		])

		);
};



// 5. Use the min operator to get the lowest price of a fruit per supplier
async function fruitsLowPrice(db) {
	return await(
			// https://www.mongodb.com/docs/manual/reference/operator/aggregation/
			// https://www.digitalocean.com/community/tutorials/how-to-use-aggregations-in-mongodb
			// The $min and $max operators are used in MongoDB's Aggregation framework to find the minimum and maximum values of a specified field, respectively, across multiple documents.
			// Add query here
		db.fruits.aggregate([
		  {
		    $group: {
		      _id: "$supplier_id",
		      min_Price: { $min: "$price" }
		    }
		  }
		])


		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
