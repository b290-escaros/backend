//Add code here

const http = require("http");
const port = 4000;

const app = http.createServer(function(req, res){
    if (req.url === "/") {
        // Respond to the root URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Welcome to Booking System");
        res.end();
    }
    else if (req.url === "/profile") {
        // Respond to the /profile URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Welcome to your profile!");
        res.end();
    }
    else if (req.url === "/courses") {
        // Respond to the /courses URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Here's our courses available");
        res.end();
    }
    else if (req.url === "/addCourse") {
        // Respond to the /addcourse URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Add a course to our resources");
        res.end();
    }
    else if (req.url === "/updateCourse") {
        // Respond to the /updatecourse URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Update a course to our resources");
        res.end();
    }
    else if (req.url === "/archiveCourses") {
        // Respond to the /archivecourses URL
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.write("Archive courses to our resources");
        res.end();
    }
    else {
        // Handle requests for unknown URLs
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.write("404 Not Found");
        res.end();
    }
});







//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;