const http = require("http");
const port = 4000;

let directory = [
		{
			"name": "Brandon",
			"email": "brandon@gmail.com"
		},
		{
			"name": "Brandy",
			"email": "brandy@gmail.com"
		}

	];


const app = http.createServer(function(req, res){
	if(req.url === "/users" && req.method === "GET"){

		// Requests the "/users" path and "GETS" information 
		// Sets the status code to 200, denoting OK
        // Sets response output to JSON data type
		res.writeHead(200, {"Content-Type" : "application/json"});
		// Input HAS to be data type STRING hence the JSON.stringify() method
        // This string input will be converted to desired output data type which has been set to JSON
        // This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
		res.write(JSON.stringify(directory));
		res.end();
	}
	if(req.url === "/users" && req.method === "POST"){
		// Declare and intialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on
		let reqBody = "";

		// A stream is a sequence of data
		// Data is received from the client and is processed in the "data" stream
		// The information provided from the request object enters a sequence called "data" the code below will be triggered
	 	// data step - this reads the "data" stream and processes it as the request body
		req.on("data", function(data){

			// Assigns the data retrieved from the data stream to requestBody
			reqBody += data;
		})
		req.on("end", function(){
			// Check if at this point the requestBody is of data type STRING
	 		// We need this to be of data type JSON to access its properties
			console.log(typeof reqBody);

			// Convert JSON to Javascript Object
			reqBody = JSON.parse(reqBody);

			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}

			// Add the new user into the mock databse
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"})
			res.write(JSON.stringify(newUser));
			res.end();
		});
	}
});

app.listen(port, () => console.log(`Server running at localhost:${port}`))