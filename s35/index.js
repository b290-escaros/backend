const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [Section] MongoDB connection

// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB

// Syntax
	// mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });

mongoose.connect("mongodb+srv://admin:admin123@zuitt.5h7tglz.mongodb.net/b290-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Connecting to MongoDB locally
// 27017 is the default port on where mongo instances are run in a device
// mongoose.connect("mongodb://localhost:27017/bXX-to-do", 
// 	{ 
// 		useNewUrlParser : true,  
// 		useUnifiedTopology : true
// 	}
// );

const db = mongoose.connection
// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "DB connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "DB connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

// [Section] Mongoose Schemas

// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}

});

// [Section] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database
// Server > Schema (blueprint) > Database > Collection

// The variable/object "Task" and "User" can now used to run commands for interacting with our database
// "Task" and "User" are both capitalized following the MVC approach for naming conventions
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const Task = mongoose.model("Task", taskSchema);


// Create new Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
// Create User model
const User = mongoose.model("User", userSchema);


// SECTION] Middlewares
// Setup for allowing the server to handle data from requests
// allows our app to read json data
app.use(express.json());
app.use(express.urlencoded({extended : true}));

// SECTION] Routes
// Creating a new task

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/newTask", (req, res) => {
	Task.findOne({name: req.body.name}).then((result, err) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task form");
		}else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save().then((savedTask, savedErr) =>{
					if(savedErr){
						return console.log(savedErr)
					}else {
						return res.status(201).send("New task created");
					}
			})
		}
	})
})

    // Getting all the tasks

	// Business Logic
	/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/

// Getting all the tasks
app.get("/tasks", (req, res) => {
	// Business Logic
	// 1. Retrieve all the documents
	Task.find()
		.then(tasks => {
			// 3. If no errors are found, send a success status back to the client/Postman and return an array of documents
			res.status(200).json(tasks);
		})
		.catch(err => {
			// 2. If an error is encountered, print the error
			console.error(err);
			return res.status(201).send("Error retrieving tasks");
		});
});


// Creating a new user
app.post("/signup", (req, res) => {
	// Business Logic
	// Check if there are duplicate users
	User.findOne({ username: req.body.username }).then(result => {
		if (result != null && result.username === req.body.username) {
			return res.send("Duplicate username found");
		} else {
			if (req.body.username && req.body.password) {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

				newUser.save().then((savedUser, savedErr) => {
					if (savedErr) {
						console.log(savedErr);
						return res.status(500).send("Error saving user");
					} else {
						return res.status(201).send("New user registered");
					}
				});
			} else {
				// Return an error message if either username or password properties are empty
				return res.send("BOTH username and password must be provided.");
			}
		}
	});
});





if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;


