

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
  name: "Ash Ketchum",
  age: 16,
  pokemon: ["Pikachu", "Charizard", "Bulbasaur"],
  friends: {
    hoem: ["Mary", "Max"],
    kanto: ["Brock", "Misty"]
  },
  talk: function() {
    return "Pikachu! I choose you!";
  }
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name); // Accessing property using dot notation
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]); // Accessing property using square bracket notation
console.log("Result of talk method:");
console.log(trainer.talk()); // Invoking the talk method using console.log()

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;

  this.tackle = function(target) {
    console.log(`${this.name} tackled ${target.name}`);
    target.health -= this.attack;
    let message = `${target.name}'s health is now reduced to ${target.health}`;

    if (target.health <= 0) {
      this.faint(target);

    } else {
      console.log(message)
      message = ""
    }

    return message
  };


  this.faint = function(target) {
    console.log(target.name + " has fainted.");
  };
}

  // return target.name + "'s health is now reduced to " + target.health;
// Create/instantiate several pokemon objects from the constructor with varying name and level properties
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu)
console.log(geodude)
console.log(mewtwo)



console.log(geodude.tackle(pikachu)); 
console.log(pikachu)
console.log(mewtwo.tackle(geodude));
console.log(geodude)




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}


