// console.log("rererr")

// Section Functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// Function Declaration 
// //(function statement) defines a function with the specified parameters.
/*
	function functionName(){
	statement;
	statement;
	statement;

	};
*/
// function keyword - used to defined a javascript functions
// functionName - the function name. Functions are named to be able to use later in the code.
// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

function printName() {
	console.log("My name is Nicolas");
};

// Section Function Invocation
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
//It is common to use the term "call a function" instead of "invoke a function".
printName();

// printAge();index.js:31 Uncaught ReferenceError: printAge is not defined function should be define


// Secction Function Declarations Vs Expressions
	// Function Declarations

	//A function can be created through function declaration by using the function keyword and adding a function name.

	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

	declaredFunction(); // Declared functions can be hoisted. as long as the function has been defined
	//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.
	function declaredFunction(){
		console.log("Hello from the other side");
	}
	declaredFunction();

// Function Expresion
	//A function can also be stored in a variable. This is called a function expression.
	//A function expression is an anonymous function assigned to the variableFunction

	// Anonimous Function - Function without a name
	// variableFunction();Uncaught ReferenceError: Cannot access 'variableFunction' before initialization
	let variableFunction = function(){
		console.log("I must have called a thousand times");
	}
	variableFunction();

	//A function expression of a function named func_name assigned to the variable funcExpression
	//How do we invoke the function expression?
	//They are always invoked (called) using the variable name.

	let funcExpression = function funcName(){
		console.log("To tell you I'm sorry");

	}

	funcExpression();
	//You can reassign declared functions and function expressions to new anonymous functions.

	declaredFunction = function(){
		console.log("Updated declared Function")
	}

	declaredFunction();

	funcExpression = function(){
		console.log("Updated declared Expression")
	}

	funcExpression();

	// constantFunction();

	//Uncaught TypeError: Assignment to constant variable.

	const constantFunction = function(){
		console.log("Initializad with const")
	};

	constantFunction();
// Section Function Scoping

/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/
 	{
 		let localVar = "Armando Perz"
 	}

 	let globalVAr = "Mr. Porgi";

 	console.log(globalVAr);
 	// console.log(localVar);index.js:112 Uncaught ReferenceError: localVar is not defined

 	function showNames(){
 		// Function scoped variables
 		var functionVar = "Arthas";
 		const functionConst = "Porthas";
 		let functionLet = "Aramis"
 	
 	console.log(functionVar);
 	console.log(functionConst);
 	console.log(functionLet);
 	};
 	showNames();
 	/* All results are NOT DEFINed*/
 	// console.log(functionVar);
 	// console.log(functionConst);
 	// console.log(functionLet);


// NESTED Functions
 	// Another function inside a function

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
		function nestedFunction2() {
			let nestedName = "Joe";
			console.log(name);
		}

	// console.log(nestedName);
	// nestedFunction();
	};
	myNewFunction();
 	// nestedFunction();

	let globalName = "Bill Gates";

	function myNewFunction2(){
		let nameInside = "Steve Jobs";
		console.log(globalName);
	}
	myNewFunction2();
	// console.log(nameInside);Function scope cannot be accessed globally


// Section Using Alert
	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.


	// function showAlert(){
	// 	alert("Hello")
	// }

	// showAlert();

	console.log("You can only see this alert when alert is dismissed")
// Section | Using prompt
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

	// let samplePrompt = prompt("Enter your name: ");
	// console.log(typeof samplePrompt);
	// console.log("Hello" + samplePrompt);

	let sampleNullPrompt = prompt("Dont't Enter Anything");
	console.log(sampleNullPrompt);

	//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

	//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

	function printWelcomeMessage(){
		let firstName = prompt("Enter your firstname")
		let lastName = prompt("Enter your lastname")

		console.log("Hello " + " "+ lastName + "!")
		console.log("Welcome to my life");
	};

	// printWelcomeMessage();

// Section Function Naming Conventions
// 
	function getCourses(){
		let courses = ["Science", "Math", "English"];
		console.log(courses)
	};
	getCourses();


	//Avoid generic names to avoid confusion within your code.

		function get(){

			let name = "Jamie";
			console.log(name);

		};

		get();

	//Avoid pointless and inappropriate function names.

		function foo(){

			console.log(25%5);

		};

		foo();

	//Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();

// Return Statements

function returnMe(){
	return true
}

let boolean = returnMe();

console.log(boolean);

function returnThis(){
	console.log(false)
}

let boolean2 = returnThis()
console.log(boolean2);