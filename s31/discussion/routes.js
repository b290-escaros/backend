const http = require("http");

// Creates variable "port" to store the port number
const port = 4001;

// Creates a variable "app" that stores the output of createServer() method.
const app = http.createServer((req, res) => {
	if(req.url == "/greeting"){
		res.writeHead(200, {"Content-Type": "text/plain"});
    	res.end("Hello again World!");
	}else if (req.url == "/homepage"){
		res.writeHead(200, {"Content-Type": "text/plain"});
    	res.end("This is the homepage");
	}else{
		res.writeHead(404, {"Content-Type": "text/plain"});
    	res.end("404: Page not found");
	}
});

// Uses the "app" and "port" variables created above
app.listen(port);
console.log(`Server now running at localhost:${port}.`);
console.log(`Server now running at localhost:${port}.`);
console.log(`Server now running at localhost:${port}.`);

