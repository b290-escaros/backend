// Use the "require" directive to load Node.js
// A "package" or "module" is a software component or part of a program that contain one or more routine/methods/function
// The "http module" lets node.js transfer data using hyper text transfer protocol
// Clients (browser) an servers (node/express) communicate by exchanging individual messages.

/*let http = require("http");

// Using this module "createServer()", method we canm create an http server that listens on a specified port.

// A port is a virtual point where network connection start and end.
// Each port is associated with specific process or serves

http.createServer(function (request, response) {
	// writeHead() method
	// Set the status code for the response - 200 -> OK/SUCCESS

	response.writeHead(200, {"Content-Type" : "text/plain"});

	// send the response with text content "Hello World"
	response.end("Hello World!");
}).listen(4000);

console.log("Server running at localhost:4000")*/

const http = require("http");

http.createServer(function (request, response) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello World!");
}).listen(4000);

console.log("Server running at localhost:4000");
