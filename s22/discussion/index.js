

// ARRAY Methods
/*
	Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	Arrays can either be mutated or iterated.

	Array mutations seek to modify the contents of an array while array iterations aim to evaluate and loop over each element in an array.
*/

// SECTION] Mutator Methods
/*
	Mutator methods are functions that "mutate" or change an array after they're created
	These methods manipulate the original array performing various tasks such as adding and removing elements
*/
let fruits = ["Apple", "Orange", "Kiwi","Dragon Fruit"];

// push()
/*
	Adds an element in the end of an array AND returns the array's length
			Syntax
				arrayName.push();
*/
console.log("Curret array: ")
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method: ")
console.log(fruits);

// Adding multiple elements using push
fruits.push("Siniguelas", "Grapes");
console.log("Mutated array from Multiple push method: ")
console.log(fruits)

// pop()
/*
	Removes the last element in an array AND returns the removed element
			Syntax
				arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method: ")
console.log(fruits);

// unshift()
/*
	Adds one or more elements at the beginning of an array
		Syntax
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', elementB);
*/
fruits.unshift("Avocado", "Papaya");
console.log("Mutated array from unshift method: ")
console.log(fruits);

// shift()
/*
	Removes an element at the beginning of an array AND returns the removed element
			Syntax
			arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ")
console.log(fruits);

// splice()
/*
	Simultaneously removes elements from a specified index number and adds elements
		Syntax
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Pineapple", "Watermelon")
console.log("Mutated array from splice method: ")
console.log(fruits);

// sort()
/*
	Rearranges the array elements in alphanumeric order
		Syntax 
			arrayName.sort();
*/
fruits.sort();
console.log("Mutated array from sort method:")
console.log(fruits);
// let mix = [0,1,"A","b","#","!",{},null];
// console.log(mix.sort());

// reverse()
/*
	Reverses the order of array elements
		Syntax
			arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method:")
console.log(fruits);

// SECTION] Non-Mutator Methods
/*
	Non-Mutator methods are functions that do not modify or change an array after they're created
	These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
*/
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
// indexOf()
/*
	Returns the index number of the first matching element found in an array
	If no match was found, the result will be -1.
	The search process will be done from first element proceeding to the last element
		Syntax
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex);
*/
console.log("Initial Array: ")
console.log(countries)

let firstIndex = countries.indexOf("PH", 0);
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry)

// lastindexOf()
/*
	Returns the index number of the last matching element found in an array
	The search process will be done from last element proceeding to the first element
		Syntax
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, fromIndex);
*/
// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex)

// Getting the index number starting from a specific index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lastIndexOf method: " + lastIndexStart)


// slice()
/*
	Portions/slices elements from an array AND returns a new array
		Syntax
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
*/
// Slicing off elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result of slice method: ")
console.log(sliceArrayA);
// Slicing off elements from a specified index to the another element
let sliceArrayB = countries.slice(2, 4);
console.log("Result of slice method: ")
console.log(sliceArrayB);
// Slicing off elements starting from the last element of an array
let sliceArrayC = countries.slice(-3);
console.log("Result of slice method: ")
console.log(sliceArrayC);

// toString()
/*
	Returns an array as a string separated by commas
		Syntax
			arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result of toString method: ")
console.log(stringArray);

// concat()
/*
	Combines two arrays and returns the combined result
		Syntax
			arrayA.concat(arrayB);
			arrayA.concat(elementA);

*/

let taskArrayA = ["Drink HTML", "eat JavaScript",];
let taskArrayB = ["Inhale CSS", "Breathe bootstrap",];
let taskArrayC = ["get git", "be node",];

let tasks = taskArrayA.concat(taskArrayB)
console.log("Result of concat method: ")
console.log(tasks)


let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log("Result of concat method: ")
console.log(allTasks)

let combinedTasks = taskArrayA.concat("smell express", "throw react")
console.log("Result of concat method: ")
console.log(combinedTasks)

// join()
/*
	Returns an array as a string separated by specified separator string
		Syntax
			arrayName.join('separatorString');
*/
let users = ["Lucy", "Edmond", "Susan", "Peter"];

// If separator is not included in join, the separator will be in comma by default
console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' | '));

// SECTION] Iteration Method
/*
	Iteration methods are loops designed to perform repetitive tasks on arrays
	Iteration methods loops over all items in an array.
	Useful for manipulating array data resulting in complex tasks
	Array iteration methods normally work with a function supplied as an argument
*/

// forEach()
/*
	Similar to a for loop that iterates on each array element.
	For each item in the array, the anonymous function passed in the forEach() method will be run.
	The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
	Variable names for arrays are normally written in the plural form of the data stored in an array
	It's common practice to use the singular form of the array content for parameter names used in array loops
	forEach() does not return anything.

		Syntax
			arrayName.forEach(function(indivElement) { statement })
*/

/*
	TIPS !
	-It's good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop
	-Creating a separate variable to store results of array iteration methods are also good practice to avoid confusion by modifying the original array
*/
allTasks.forEach(function(task){
	console.log(task) //for checking the elemts u iterate
});
let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length > 10){

		filteredTasks.push(task);
	}
});

console.log("Result of filtered task:  ");
console.log(filteredTasks);

// map()
/*
	Iterates on each element AND returns new array with different values depending on the result of the function's operation
	This is useful for performing tasks where mutating/changing the elements are required
	Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
		Syntax
			let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	console.log(number)//for checking the elemts u iterate
	return number * number;
})

console.log("Original Array:");
console.log(numbers);
console.log("Result of map method")
console.log(numberMap);

// every()
/*
	-Checks if all elements in an array meet the given condition
	This is useful for validating data stored in arrays especially when dealing with large amounts of data
	-Returns a true value if all elements meet the condition and false if otherwise
		Syntax
			let/const resultArray = arrayName.every(function(indivElement) { return expression/condition; })
*/

let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log("Result of 'every' method:");
console.log(allValid);
// some()
/*
	-Checks if at least one element in the array meets the given condition
	Returns a true value if at least one element meets the condition and false if otherwise
		Syntax
			let/const resultArray = arrayName.some(function(indivElement) { return expression/condition; })
*/
let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Result of 'some' method:");
console.log(someValid);

if (someValid){
	console.log("Some numbers in the array are greater than 2");
};

// filter()
/*
	-Returns new array that contains elements which meets the given condition
	-Returns an empty array if no elements were found
	-Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
	-Mastery of loops can help us work effectively by reducing the amount of code we use
	-Several array iteration methods may be used to perform the same result
		Syntax
			let/const resultArray = arrayName.filter(function(indivElement) { return expression/condition; })
*/

let filterValid = numbers.filter(function(number){
	return(number < 3);
});
console.log("Result of 'filter' method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log("Result of 'filter' method:");
console.log(nothingFound);


let filteredNumbers = [];
numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number)
	}
});
console.log("Result of 'foreach' without using filter method:");
console.log(filteredNumbers);

// includes()
/*
	includes() method checks if the argument passed can be found in the array.
	it returns a boolean which can be saved in a variable.
	returns true if the argument is found in the array.
	returns false if it is not.
		Syntax: 
			arrayName.includes()

*/

let products = ["Mouse", "Keyboards", "Laptops", "Monitor"];

let productFound1 = products.includes("Mouse")
console.log(productFound1);

let productFound2 = products.includes("Headset")
console.log(productFound2);

/*
	Methods can be "chained" using them one after another
	The result of the first method is used on the second method until all "chained" methods have been resolved. -forEach() cannot be chained as it does not return anything.
	How chaining resolves in our example:
	The "product" element will be converted into all lowercase letters
	The resulting lowercased string is used in the "includes" method
*/
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});
console.log(filteredProducts);

// reduce()
/*
	Evaluates elements from left to right and returns/reduces the array into a single value
		Syntax
			let/const resultArray = arrayName.reduce(function(accumulator, currentValue) { return expression/operation })

	-The "accumulator" parameter in the function stores the result for every iteration of the loop
	-The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	How the "reduce" method works
	1. The first/result element in the array is stored in the "accumulator" parameter
	2. The second/next element in the array is stored in the "currentValue" parameter
	3. An operation is performed on the two elements
	4. The loop repeats step 1-3 until all elements have been worked on

*/

let iteration = 0;

let reduceArray = numbers.reduce(function(acc, curr){
	console.warn("Current Iteration:" + ++iteration);
	console.log("accumulator: " + acc);
	console.log("Current value: " + curr);

	return acc + curr

})
console.log(numbers)
console.log("Result of the reduce method: " + reduceArray)
console.log(reduceArray)

let list = ["Hello", "Again", "World"];
let reducedJoin = list.reduce(function(acc, curr){
	return acc + ' ' + curr
})

console.log("Result of the reduce method: " + reducedJoin)